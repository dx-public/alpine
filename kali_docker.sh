#!/bin/bash

echo "Checking if Docker is installed"
installed=`dpkg-query -l | grep 'docker.io'`

if [ ! -z "$installed" ]; then
echo "Docker is aleady installed"
echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
read ans;

case $ans in
    r|R)
        installed='';;
    u|U)
        echo "uninstalling"
        apt -y remove docker.io*;;
    *)
        installed='n';;
esac

fi

if [ -z "$installed" ]; then

echo "Installing Docker"
sudo apt -y install docker.io


fi


echo "DONE going back to menu"
