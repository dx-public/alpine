#!/bin/bash

echo "Checking if SSH is installed"
installed=`dpkg-query -l | grep 'openssh-server'`

if [ ! -z "$installed" ]; then
echo "OpenSSH is aleady installed"
echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
read ans;

case $ans in
    r|R)
        installed='';;
    u|U)
        echo "disabling"
        sudo systemctl disable ssh;;
    *)
        installed='n';;
esac

fi

if [ -z "$installed" ]; then

echo "Installing SSH Server"
sudo apt -y install openssh-server
sudo apt-mark auto openssh-server

echo "Removing Previous SSH Defaults"
sudo update-rc.d -f ssh remove

echo "Updating SSH Defaults"
sudo update-rc.d -f ssh defaults

echo "Updating Keys"
sudo mkdir -p /etc/ssh/insecure_original_default_kali_keys
sudo mv /etc/ssh/ssh_host_* /etc/ssh/insecure_original_default_kali_keys/
sudo dpkg-reconfigure openssh-server
echo "change line PermitRootLogin without-password if applicable (yes)"

echo "Restarting SSH"
sudo service ssh restart
sudo update-rc.d -f ssh enable 2 3 4 5

echo "Feel Free to change the MOTD (/etc/motd) if you want and then reboot ssh server"
fi

echo "Checking if kdig is installed"
installed=`dpkg-query -l | grep 'knot-dnsutils'`
if [ -z "$installed" ]; then apt -y install knot-dnsutils; fi

echo "Updating SSH Public key"
#ssh-keygen -t rsa -f dnskey -b 4096; cat dnskey.pub
pubkey=`kdig @9.9.9.9 +tls +short TXT key-ssh-rsa.$(hostname -f | cut -d'.' -f2-) | sed -e 's/" "//g' | tr -d '\n' | sed '$s/$/\n/' | tr -d '"'`
echo "checking key"
asum=`echo $pubkey | awk '{ print $1,$2 }' | sha256sum | awk '{ print $1 }'`
psum=`echo $pubkey | awk '{ print $3 }' | cut -d'-' -f1`
if [ "$psum"=="$asum" ]; then 
mkdir -p ~/.ssh/
echo $pubkey > ~/.ssh/authorized_keys
fi


echo "Checking if Fail2Ban is installed"
installed=`dpkg-query -l | grep 'fail2ban'`

if [ -z "$installed" ]; then
echo "Installing Fail2ban..."
sudo apt update && sudo apt -y install fail2ban
fi
sudo rm /etc/fail2ban/jail.d/default*
sudo tee /etc/fail2ban/jail.d/ssh.conf<<EOF
# Block attempts to brute force SSH logins
[sshd] 
enabled = true 
port = ssh 
filter = sshd
logpath = /var/log/auth.log 
maxretry = 1
bantime = 86100
ignoreip = 127.0.0.1 ::1
EOF
echo "starting fail2ban service"
sudo systemctl enable fail2ban.service
sudo systemctl restart fail2ban.service
sleep 2
sudo fail2ban-client status sshd
echo "to unban: sudo fail2ban-client set sshd unbanip 127.0.0.1"

echo "DONE going back to menu"
