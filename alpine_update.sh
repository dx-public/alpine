#!/bin/env bash

#Tweaks to free space
journalctl --vacuum-size=500M


echo "-----------------"
echo "Checking if online"
echo "-----------------"
if curl -sI --fail https://nist.time.gov -o /dev/null; then
 echo "ONLINE... continuing..."
else
 echo "OFFLINE... exiting..."
 exit
fi

echo "-----------------"
echo "Sync Time"
echo "-----------------"
timedatectl set-ntp false
#date -s "`curl --connect-timeout 4.1 -sI 'https://time.cloudflare-dns.com/' 2>/dev/null | grep -i '^date:' | sed 's/^[Dd]ate: //g'`"
date -s "`curl --connect-timeout 4.1 -sI https://nist.time.gov/timezone.cgi?UTC/s/0 | awk -F': ' '/Date: / {print $2}'`"
hwclock -w
timedatectl set-local-rtc 0
timedatectl

echo "-----------------"
echo "Securing packages"
echo "-----------------"
sed -i 's/http.kali.org/kali.download/g' /etc/apt/sources.list
find /etc/apt/sources.list* -type f -exec sed -i -e 's/http:/https:/g' {} 2>/dev/null \;

echo "------------------"
echo "Getting keys"
echo "------------------"
#sudo apt update 2>&1 1>/dev/null | sed -ne 's/.*NO_PUBKEY //p' | while read key; do if ! [[ ${keys[*]} =~ "$key" ]]; then sudo gpg  --keyserver hkps://keyring.debian.org --recv-keys "$key"; keys+=("$key"); fi; done

echo "---------------------------"
echo "Updating Packages from repo"
echo "---------------------------"
apt -y update

echo "-------------------"
echo "Fix Missing Depends"
echo "-------------------"
apt -y --fix-broken install

echo "------------------"
echo "Upgrading Packages"
echo "------------------"
#sudo apt -y full-upgrade
DEBIAN_FRONTEND=noninteractive apt -o Dpkg::Options::="--force-confnew" -y full-upgrade

echo "---------------------"
echo "Removing Old Packages"
echo "---------------------"
apt -y autoremove

if which unattended-upgrades >/dev/null; then
 echo "unattended-upgrades exist"
else
 #unattended-upgrades
 echo -n "Keep Packages Updated? (y/N) "
 read ans;

 case $ans in
    y|Y)
        apt -y install unattended-upgrades;;
    n|N)
        apt -y remove unattended-upgrades;;
    *)
        echo "No update";;
 esac
fi

echo "Changing Static Crontab Values"
minh=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
mind=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
minw=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
minm=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/urandom)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
sed -e "s/17/$minh/g" -e "s/25/$mind/g" -e "s/47/$minw/g" -e "s/52/$minm/g" /etc/crontab > /tmp/crontab && mv /tmp/crontab /etc/crontab

mkdir -p /etc/cron.startup
if ! grep -q '@reboot' /etc/crontab; then echo '@reboot root cd / && run-parts --report /etc/cron.startup' >> /etc/crontab; fi
if ! grep -q -i 'MAILTO=' /etc/crontab; then sed -i '1i\MAILTO=""\' /etc/crontab; fi

#daily update
mkdir -p /etc/cron.daily
cfile=`realpath $0`
cat > /etc/cron.daily/update-system <<EOF
#!/bin/env bash
echo "1" | sh $cfile
EOF
chmod +x /etc/cron.daily/update-system

#weekly reboot
mkdir -p /etc/cron.weekly
cfile=`realpath $0`
cat > /etc/cron.weekly/update-system-reboot <<EOF
#!/bin/env bash
echo "1" | sh $cfile
#halt --reboot
echo b > /proc/sysrq-trigger
EOF
chmod +x /etc/cron.weekly/update-system-reboot

#Startup and sync time
mkdir -p /etc/cron.startup
cfile=`realpath $0`
cat > /etc/cron.startup/update-time <<EOF
#!/bin/env bash
echo "Syncing Time"
timedatectl set-ntp false
date -s "\$(curl --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 -sI 'https://time.cloudflare-dns.com/' 2>/dev/null | grep -i '^date:' | sed 's/^[Dd]ate: //g')"
date -s "\$(curl --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 -sI https://nist.time.gov/timezone.cgi?UTC/s/0 | awk -F': ' '/Date: / {print \$2}')"
EOF
chmod +x /etc/cron.startup/update-time

#make all packages restart service
echo '* libraries/restart-without-asking boolean true' | debconf-set-selections

if which needrestart >/dev/null; then
#check for reboot
sed -i 's/#$nrconf{restart} = \x27i\x27/$nrconf{restart} = \x27a\x27/g' /etc/needrestart/needrestart.conf
DEBIAN_FRONTEND=noninteractive needrestart -r l
else
apt -y install needrestart
fi

#[ -e /var/run/reboot-required ] && echo "******REBOOT NEEDED******"
if [ -e /var/run/reboot-required ]; then
 #reboot
 echo -n "Reboot Needed... Reboot? (Y/n) "
 read ans;
 case $ans in
    y|Y)
        echo "Rebooting..." & halt --reboot;;
    n|N)
        echo "******PLEASE REBOOT******";;
    *)
        echo "Rebooting..." & echo b > /proc/sysrq-trigger;;
 esac
 fi
