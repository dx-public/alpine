#!/bin/env bash

#echo "-----------------"
#echo "Sync Time"
#echo "-----------------"
#timedatectl set-ntp false
#date -s "`curl --connect-timeout 2.1 -sI 'https://time.cloudflare-dns.com/' 2>/dev/null | grep -i '^date:' | sed 's/^[Dd]ate: //g'`"
#date -s "`curl --connect-timeout 2.1 -sI https://nist.time.gov/timezone.cgi?UTC/s/0 | awk -F': ' '/Date: / {print $2}'`"
#echo ""
#echo ""
#echo ""

#get current dir
cdir=`dirname $0`
#get current file
fnme=`basename $0`

#check status
if curl -sI --fail https://git-scm.com -o /dev/null; then
 [ $(git rev-parse HEAD) = $(git ls-remote $(git rev-parse --abbrev-ref @{u} | sed 's/\// /g') | cut -f1) ] && changed=0 || changed=1
 if [ $changed = 1 ]; then
  #update git-repo first
  #git reset -- hard # for resetting
  echo "Device online... Updating script..."
  git -C $cdir pull
  echo "Please rerun ${fnme}"
  exit
 fi
else
echo "Unable to update repo... you are offline... continuing..."
fi

ls /root/ 1>/dev/null 2>&1 || (echo "Please run as root" && exit)

# Running a forever loop using while statement
# This loop will run untill select the exit option.
# User will be asked to select option again and again
while :
do

# creating a menu with the following options
echo "--------------"
echo "Bootstrap Menu"
echo "--------------"
echo "01. Update packages"
echo "02. Enable/Disable SSH Server"
echo "03. Install Microsoft Edge"
echo "04. Install Google Chrome"
echo "05. Install Brave Browser"
echo "06. Install Docker"
echo "07. Install VirtualBox"
echo "08. Enable/Disable GUI Server"
echo "09. Enable/Disable GUI Win (kex)"
echo "10. Change Hostname"
echo -n "98. Reboot"; [ -e /var/run/reboot-required ] && echo " (NEEDED) " || echo ""
echo "99. Exit menu "
echo -n "Enter your menu choice [#]: "

# reading choice
read choice

# case statement is used to compare one value with the multiple cases.
case $choice in
  # Pattern 1
  01|1)  echo "You have selected the option 1"
      echo "Updating Packages... "
      sh $cdir/alpine_update.sh;;
  # Pattern 2
  02|2)  echo "You have selected the option 2"
      echo "Running install script... alpine_sshserver.sh "
      sh $cdir/alpine_sshserver.sh;;
  # Pattern 3
  03|3)  echo "You have selected the option 3"
      echo "Running install script... alpine_edge.sh "
      sh $cdir/alpine_edge.sh;;
  # Pattern 4
  04|4)  echo "You have selected the option 4"
      echo "Running install script... alpine_chrome.sh "
      sh $cdir/alpine_chrome.sh;;
  # Pattern 5  
  05|5)  echo "You have selected the option 5"
      echo "Running install script... alpine_brave.sh "
      sh $cdir/alpine_brave.sh;;
  # Pattern 6  
  06|6)  echo "You have selected the option 6"
      echo "Running install script... alpine_docker.sh "
      sh $cdir/alpine_docker.sh;;
  # Pattern 7
  07|7)  echo "You have selected the option 7"
      echo "Running install script... alpine_virtualbox.sh.sh "
      sh $cdir/alpine_virtualbox.sh;;
  # Pattern 8
  08|8)  echo "You have selected the option 8"
      echo "Running install script... alpine_guiserver.sh "
      sh $cdir/alpine_guiserver.sh;;
  # Pattern 9
  09|9)  echo "You have selected the option 9"
      echo "Running install script... alpine_kex.sh"
      sh $cdir/alpine_kex.sh;;
  # Pattern 10
  10)  echo "You have selected the option 10"
      echo "Running install script... alpine_hostname.sh "
      sh $cdir/alpine_hostname.sh;;
  # Pattern 97
  97|R) echo "Force Rebooting..."
       echo b > /proc/sysrq-trigger;;
  # Pattern 98
  98|r)  echo "Rebooting ..."
       sudo halt --reboot;;
  # Pattern 99
  99|q|'')  echo "Quitting ..."
       exit;;
  # Default Pattern
  *) echo "" && echo "invalid option" && echo "";;

esac
  #echo -n "Enter your menu choice [#]: "
done
