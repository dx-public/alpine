#!/bin/bash

echo "Checking if Kex is installed"
binaryfound=`which "kex"`
if [ -z "$binaryfound" ]; then 
 apt update
 apt install -y kali-win-kex
fi

echo "starting kex with sound"
kex --win -s
