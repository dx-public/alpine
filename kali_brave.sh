#!/bin/bash

echo "Checking if Brave is installed"
installed=`dpkg-query -l | grep 'brave-browser'`

if [ ! -z "$installed" ]
then
echo "Brave is aleady installed"
echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
read ans;

case $ans in
    r|R)
        installed='';;
    u|U)
        echo "uninstalling"
        apt -y remove brave-browser*;;
    *)
        installed='n';;
esac

fi

if [ -z "$installed" ]; then

echo "Installing utils"
apt install -y apt-transport-https ca-certificates curl software-properties-common wget

echo "Getting Keys"
curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "Removing old repos"
rm /etc/apt/sources.list.d/brave-browser-release*.list

echo "Add Repo"
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"| tee /etc/apt/sources.list.d/brave-browser-release.list
chattr +i /etc/apt/sources.list.d/brave-browser-release.list
fi

echo "Update Repo"
apt update

echo "Installing Brave"
apt install -y brave-browser

echo "DONE going back to menu"
