#!/bin/bash

echo "Checking if Chrome Remote Desktop is installed"
chromeremotedesktopinstalled=`dpkg-query -l | grep 'chrome-remote-desktop'`

if [ ! -z "$chromeremotedesktopinstalled" ]; then
echo "Chrome Remote Desktop is aleady installed"
echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
read ans;

case $ans in
    r|R)
        chromeremotedesktopinstalled='';;
    u|U)
        echo "uninstalling"
        apt -y remove chrome-remote-desktop*;;
    *)
        chromeremotedesktopinstalled='n';;
esac

fi

if [ -z "$chromeremotedesktopinstalled" ]; then

echo "Downloadng Google Chrome Remote Desktop"
curl -s -L https://dl.google.com/linux/direct/chrome-remote-desktop_current_amd64.deb -o /tmp/chromeremotedesktop.deb

echo "Installing Chrome Remote Desktop"
sudo dpkg -i /tmp/chromeremotedesktop.deb

fi






echo "Checking if NoVNC is installed"
novncinstalled=`dpkg-query -l | grep 'novnc'`

if [ ! -z "$novncinstalled" ]; then
echo "NoVNC is aleady installed"
echo -n "Do you want to (r)einstall/(u)ninstall/(n)othing? (r/u/N)"
read ans;

case $ans in
    r|R)
        novncinstalled='';;
    u|U)
        echo "uninstalling"
        apt -y remove novnc*;;
    *)
        novncinstalled='n';;
esac

fi

if [ -z "$novncinstalled" ]; then

echo "Installing vnc"
sudo apt install -y novnc x11vnc


#echo "Show listening"
#ss -antp | grep vnc

echo "create certificate"
openssl req -new -x509 -days 365 -nodes -out /usr/share/novnc/self.pem -keyout /usr/share/novnc/self.pem

echo "Enabling Remote SSH"
sudo systemctl enable ssh --now

fi

echo "Running vnc service"
#ps wwwwaux | grep '\-auth' 
x11vnc -display :0 -localhost -rfbport 5900 -nopw -bg -xkb -ncache -ncache_cr -quiet -nevershared -forever -auth /var/run/lightdm/root/:0
echo ""
echo "Run: ssh <username>@<host> -L 8081:localhost:8081 to connect remotely"
echo ""
echo "Starting HTML5 version"
/usr/share/novnc/utils/launch.sh --listen 8081 --vnc localhost:5900


echo "DONE going back to menu"
