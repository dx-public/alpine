# Alpine
Bootstrap Alpine Linux

## Install

```
cd ~
git clone https://gitlab.com/dx-public/alpine.git
cd alpine
sh alpine-menu.sh
```

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/dx-public/alpine.git
git branch -M main
git push -uf origin main
```
